#______________[ INITIALIZATION ]
# NEW USERS
http https://auth.anjanahelp.org/v1/auth/register username="user1" password="user1" -v
http https://auth.anjanahelp.org/v1/auth/register username="admin" password="admin" -v

# LOGIN AS USER1
http https://auth.anjanahelp.org/v1/auth/validate username="user1" password="user1" --session /tmp/cookie -v 

# ADD NEW ORG
http https://org.anjanahelp.org/v1/org/add  orgname="org1" email="julio.cesar.guerrero.olea@gmail.com" type="immigrant"  -v --verify=no
http https://org.anjanahelp.org/v1/org/add  orgname="org2" email="mushi.the.moshi.man@gmail.com"       type="immigrant"  -v --verify=no

# ADD NEW CASE
# http api.anjanahelp.org/v1/case/add useremail="externaluser@gmail.com" type="immigrant" --session /tmp/cookie -v
# http api.anjanahelp.org/v1/case/add useremail="baruyi@mail.com"        type="immigrant" --session /tmp/cookie -v

# ADD NEW EMAIL
# http api.anjanahelp.org/v1/mail/new type="immigrant" useremail="t@email.com" agentemail="d@gmail.com" --session /tmp/cookie

#_______________[ LOGIN FLOW ]
## GET TOKEN
http https://auth.anjanahelp.org/v1/auth/validate username="admin" password="admin" --session /tmp/cookie -v

## LOGIN
http https://auth.anjanahelp.org/v1/auth/login --session /tmp/cookie
