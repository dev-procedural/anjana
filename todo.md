***Please check todos in code***

## Deploy
- [X] create swagger ui
   - https://hub.docker.com/r/swaggerapi/swagger-ui
- [ ] create DEV/PROD parity
- [ ] create CICD strategy
    - [ ] create kustomize layout
    - [ ] tekton ci
    - [ ] argo cd
- [ ] optimize pv's for dev(local) and prod

## Backend
### auth
- [ ] complete token validation
- [ ] complete RBAC
   - add group validation
   - auth middleware to validate

### mail
- [X] missing to add html template 
- [X] change the way user collection happens
- [X] add wording for accepting and rejecting in module and template

### org
- [X] add phase status update to cases
- [ ] complete missing endpoints to achieve CRUD
- [ ] auth middleware RBAC

### testing and doc
- [X] complete swagger doc
- [ ] improve swagger doc
   - [ ] add objects in errors
   - [ ] add objects in response
- [ ] complete unit tests for endpoints
   - [ ] add main test
   - [ ] add pkg test
   - [ ] add db test


### SEC
- [X] add auth0 as jwks
- [X] add certificates
- [ ] add mutual certs
- [ ] add p2p auth policies
- [ ] add svc accounts 

### DR
- https://velero.io/

### parametrization 
- [ ] add viper
- [ ] parametrize more mail elements
- [ ] find missing urls parametrized

### complete flow
- [ ] complete org 
   - [ ] complete missing endpoints to achieve CRUD
- [ ] complete auth 
   - [ ] complete DB CRUD
   - [ ] complete missing endpoints to achieve CRUD

###obs goodies
- [X] observability
- [ ] tracing

## Frontend
### replan frontend design
- [ ] add callback view for routing RBAC
- [ ] add verbosity on errors
- [ ] add form validation
- [ ] add missing views
   - [ ] fix TOS
   - [ ] map view
- mocks https://mockapi.io/

### QA
- [ ] BDD ginkgo 
- [ ] E2E tests colly 
