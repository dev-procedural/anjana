# Anjana is an opensource tool to get a probono agent to help you

[components](/images/components.png "Components")  
[time](/images/timeuml.png "Time")  

## Dependencies
- docker
- docker-compose
- go 1.17
- taskfile
- k3d

## Instructions to run
### Create cluster and services
```
task all
```

### Turn on
```
task up
```

### Turn off
```
task del
```

