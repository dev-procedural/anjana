### create overlay and gitops pipeline

### create ansible layout
-  https://stackoverflow.com/questions/65091430/kubernetes-use-environment-variable-configmap-in-persistentvolume-host-path
### add nates config user/admin
curl -fSl https://nats-io.github.io/k8s/setup/nsc-setup.sh | sh
curl -L https://raw.githubusercontent.com/nats-io/nsc/master/install.sh | sh -
curl -LO https://github.com/nats-io/natscli/releases/download/v0.0.33/nats-0.0.33-linux-amd64.zip

### troubleshooting
- istioctl pc listener deploy/istio-ingressgateway -n istio-system
- istioctl analyse
- istioctl ps


### update istio-ingressgateway manifest
- kubectl edit svc istio-ingressgateway -n istio-system
```
  - name: tcp-mq
    nodePort: 30599
    port: 4222
    protocol: TCP
    targetPort: 4222
```
