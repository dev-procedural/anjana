package cases

import (
	"github.com/gin-gonic/gin"
)

func CasesRouter(Cases CasesHandlerInterface, r *gin.Engine) error {
	// public router
	cases_public := r.Group("/v1/case")
	// cases_public.Use(middleware.AuthMiddleware())
	{
		cases_public.POST("/test", Cases.Dummy)
		cases_public.POST("/add", Cases.AddCase)
	}

	// private router
	// cases_private.Use(middleware.AuthMiddleware())
	cases_private := r.Group("/v1/case")
	{
		cases_private.PUT("/update/:id", Cases.UpdateCase)
		cases_private.POST("/get", Cases.GetCase)
		cases_private.DELETE("/delete", Cases.DeleteCase)
		cases_private.POST("/type", Cases.GetTypeCase)
		cases_private.POST("/types", Cases.GetTypeCases)
	}
	return nil
}
