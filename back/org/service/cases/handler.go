package cases

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"micro/models"
	"strconv"
	"strings"

	"micro/config"
	"micro/db"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/nats-io/nats.go"
)

type CasesHandler struct {
	DB db.CasesDBLayer
}

type CasesHandlerInterface interface {
	AddCase(c *gin.Context)
	GetCase(c *gin.Context)
	GetCases(c *gin.Context)
	GetTypeCases(c *gin.Context)
	GetTypeCase(c *gin.Context)
	DeleteCase(c *gin.Context)
	UpdateCase(c *gin.Context)
	Dummy(c *gin.Context)
}

func NewCasesHandler() (CasesHandlerInterface, error) {

	CONF := config.DBConfig()

	db, err := db.NewORM(CONF.DBconf())
	db.AutoMigrate(&models.Cases{})

	if err != nil {
		return nil, err
	}

	return &CasesHandler{DB: db}, nil
}

func ERR(err error, c *gin.Context) {
	if err != nil {
		c.JSON(http.StatusNotFound,
			gin.H{
				"error": err.Error(),
				"stage": "1",
			})
		return
	}
}

// ________________________________[[  CASE  ]]
// PRODUCER
// @title Org api
// @BasePath /v1/case
// mail godoc
// @Summary add new case
// @Schemes http
// @Description add new case and send notification, also this is the producer for the consumer
// @Tags        case
// @Accept      json
// @Produce     json
// @Param  type query string true "unknown, family legal, civil matter, criminal defence, civil rights, immigrant, indigenous"
// @Param  useremail query string true "useremail"
// @Param  cookie query string true "cookie bearer token"
// @Success     200 {array} models.Cases
// @Failure     400 {string} ERROR
// @Router      /v1/case/add [post]
func (h *CasesHandler) AddCase(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var cases models.Cases
	// TODO remove cookie from call, import it from middleware
	cookie, _ := c.Cookie("jwtTokenSession")

	err := c.ShouldBindJSON(&cases)
	if err != nil {
		c.JSON(http.StatusNotFound,
			gin.H{
				"error": err.Error(),
				"stage": "1",
			})
		return
	}

	_, err = h.DB.GetCase(cases)

	// TODO add case limit
	if err == nil {
		c.JSON(http.StatusNotFound,
			gin.H{
				"error": "[?] ERR: user already have a case in db",
				"stage": "1",
			})
		return
	}

	cases.Stage = "OPEN"
	case_, _ := json.Marshal(cases)
	nc, _ := nats.Connect("nats://anjanamq:4222")

	log.Println(case_)

	res := nc.Publish("NewCases", []byte(string(case_)))
	if res != nil {
		c.JSON(http.StatusNotFound,
			gin.H{
				"error": res.Error(),
				"stage": "1",
			})
		return
	}

	cookie = strings.Split(c.Request.Header["Authorization"][0], " ")[1]
	err = NotifyEmail(case_, cookie, c)
	if err != nil {
		// TODO force replay
		log.Printf("%#v", err)
		c.JSON(http.StatusNotFound,
			gin.H{
				"error": "Mail can't be sent or token not working",
				"msg":   fmt.Sprint(err),
				"stage": "1",
			})
		return
	}

	log.Printf("[!] Added and Notified %v\n", string(case_))
	c.JSON(http.StatusOK, cases)
}

func NotifyEmail(CASE []uint8, COOKIE string, c *gin.Context) error {

	JSON := bytes.NewReader(CASE)
	client := &http.Client{}
	CONF := config.ServiceConfig()

	req, _ := http.NewRequest("POST", CONF.MAIL_NEW_ENDPOINT, JSON)
	req.Header.Set("Cookie", "jwtTokenSession="+COOKIE)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+COOKIE)

	log.Printf("[!] CASE-MAIL: JWTTOKENSESSION: %#v", COOKIE)
	resp, err := client.Do(req)

	// body_ := new(bytes.Buffer)
	// body_.ReadFrom(resp.Body)
	// body := body_.String()

	var body_ bytes.Buffer
	io.Copy(&body_, resp.Body)
	body := body_.String()

	if err != nil || resp.StatusCode == 404 {
		log.Printf("[1] notify email error: err: %#v, resp testA: %#v", err, body)
		log.Printf("[B] check if orgs table is populated")
		return errors.New(body)
	}
	return nil
}

// @BasePath /v1/case
// mail godoc
// @Summary add new case
// @Schemes http
// @Description get cases by type
// @Tags        case
// @Accept      json
// @Produce     json
// @Param  type query string true "organization type"
// @Param  useremail query string true "useremail"
// @Param  cookie query string true "cookie bearer token"
// @Success     200 {array} []models.Cases
// @Failure     400 {string} ERROR
// @Router      /v1/case/get/types [post]
func (h *CasesHandler) GetTypeCases(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var cases models.Cases

	err := c.ShouldBindJSON(&cases)
	if err != nil {
		c.JSON(http.StatusNotFound,
			gin.H{
				"error": err.Error(),
				"stage": "1",
			})
		return
	}

	CASES, err := h.DB.GetTypeCases(cases.Type)
	if err != nil {
		c.JSON(http.StatusNotFound,
			gin.H{
				"error": err.Error(),
				"stage": "2",
			})
		return
	}

	log.Println("[!] ", CASES)
	c.JSON(http.StatusOK, CASES)

}

// @BasePath /v1/case
// mail godoc
// @Summary add new case
// @Schemes http
// @Description get only 1 case by type
// @Tags        case
// @Accept      json
// @Produce     json
// @Param  type query string true "organization type"
// @Param  useremail query string true "useremail"
// @Param  cookie query string true "cookie bearer token"
// @Success     200 {array} models.Cases
// @Failure     400 {string} ERROR
// @Router      /v1/case/get/type [post]
func (h *CasesHandler) GetTypeCase(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	header := c.Request.Header["Flag"][0]
	log.Printf("[?] %#v", header)

	var cases models.Cases

	err := c.ShouldBindJSON(&cases)
	if err != nil {
		c.JSON(http.StatusNotFound,
			gin.H{
				"error": err.Error(),
				"stage": "1",
			})
		return
	}

	if cases.UserEmail == "" {
		c.JSON(http.StatusNotFound,
			gin.H{
				"error": "username empty",
				"stage": "1",
			})
		return
	}

	CASE, err := h.DB.GetTypeCase(cases)
	if err != nil {
		c.JSON(http.StatusNotFound,
			gin.H{
				"error": err.Error(),
				"stage": "2",
			})
		return
	}

	c.JSON(http.StatusOK, gin.H{"msg": CASE})

}

// TODO FILL THIS
func (h *CasesHandler) GetCase(c *gin.Context) {
	return
}

func (h *CasesHandler) GetCases(c *gin.Context) {
	return
}

func (h *CasesHandler) DeleteCase(c *gin.Context) {
	return
}

// @BasePath /v1/case/
// mail godoc
// @Summary update case
// @Schemes http
// @Description update case with agent
// @Tags        case
// @Accept      json
// @Produce     json
// @Param  type query string true "organization type"
// @Param  useremail query string true "useremail"
// @Param  agent query string true "agentemail"
// @Param  cookie query string true "cookie bearer token"
// @Success     200 {array} models.Cases
// @Failure     400 {string} ERROR
// @Router      /v1/case/update/{id} [put]
func (h *CasesHandler) UpdateCase(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error"})
		return
	}

	var req models.Cases //get request
	err := c.ShouldBindJSON(&req)
	log.Printf("%#v \n", req)
	if err != nil {
		ERR := err.Error()
		c.JSON(http.StatusNotFound, gin.H{"error": ERR, "stage": 1})
		return
	}

	req.ID, _ = strconv.Atoi(c.Param("id"))
	CASE, err := h.DB.GetCase(req)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": 2})
		return
	}

	log.Printf("[!] REQ %#v", req)
	log.Printf("[!] CASE%#v", CASE)

	CASE.AgentEmail = req.AgentEmail
	CASE.Stage = req.Stage
	h.DB.UpdateCase(CASE)
	c.JSON(http.StatusOK, gin.H{"msg": fmt.Sprintf("User %s updated", CASE.AgentEmail)})
}

func (h *CasesHandler) Dummy(c *gin.Context) {

	return
}
