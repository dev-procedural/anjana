package main

import (
	"encoding/json"
	"fmt"
	"log"
	"micro/config"
	"micro/db"
	"micro/models"
	"sync"

	"github.com/nats-io/nats.go"
)

type CaseHandler struct {
	DB db.CasesDBLayer
}

func init() {
}

func CaseConsumer() {

	CONF := config.DBConfig()
	db, err := db.NewORM(CONF.DBconf())
	if err != nil {
		return
	}

	DB := &CaseHandler{DB: db}

	nc, _ := nats.Connect("nats://anjanamq:4222")
	defer nc.Close()

	// Use a WaitGroup to wait for a message to arrive
	wg := sync.WaitGroup{}
	wg.Add(1)

	// Subscribe
	if _, err := nc.Subscribe("NewCases", func(m *nats.Msg) {

		NEW_CASE := models.Cases{}

		json.Unmarshal([]byte(string(m.Data)), &NEW_CASE)

		RESULT, err := DB.DB.AddCase(NEW_CASE)
		if err != nil {
			return
		}

		log.Printf("[!] msg %v", RESULT)
		log.Printf("[!] msg %v", NEW_CASE)
		wg.Done()
	}); err != nil {
		log.Fatal(err)
	}

	// Wait for a message to come in
	wg.Wait()

}

func main() {
	forever := make(chan bool)

	go func() {
		for {
			CaseConsumer()
		}
	}()

	fmt.Printf("[!] Consumer runnning in background")
	<-forever
}
