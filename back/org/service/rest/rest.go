package rest

import (
	. "micro/service/cases"
	. "micro/service/orgs"

	_ "micro/docs"
	"micro/models"

	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func RunAPI() (*gin.Engine, error) {
	orgs, err := NewOrgHandler()
	if err != nil {
		return nil, err
	}

	cases, err := NewCasesHandler()
	if err != nil {
		return nil, err
	}
	return RunAPIWithHandler(orgs, cases), nil
}

func RunAPIWithHandler(Orgs OrgHandlerInterface,
	Cases CasesHandlerInterface) *gin.Engine {

	//Get gin's default engine
	r := gin.Default()

	r.Use(gin.Logger())
	r.Use(cors.AllowAll())
	r.Use(gin.LoggerWithFormatter(models.CustomLog))

	OrgsRouter(Orgs, r)
	CasesRouter(Cases, r)

	url := ginSwagger.URL("http://api.anjanahelp.org/org/swagger/doc.json")
	r.GET("/org/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return r
}
