package orgs

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"micro/models"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func RunAPIWithHandler(Orgs OrgHandlerInterface) *gin.Engine {
	//Get gin's default engine
	r := gin.Default()
	OrgsRouter(Orgs, r)
	return r
}

func TestHandler_GetAllOrgs(t *testing.T) {
	// Switch to test mode so you don't get such noisy output
	gin.SetMode(gin.TestMode)

	org, _ := NewOrgHandler()
	API := RunAPIWithHandler(org)

	ts := httptest.NewServer(API)

	resp, err := http.Get(fmt.Sprintf("%s/v1/org/all", ts.URL))
	defer resp.Body.Close()

	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	data, _ := ioutil.ReadAll(resp.Body)

	var orgs []models.Orgs
	json.Unmarshal(data, &orgs)

	log.Printf("%#v", len(orgs))
	assert.Equal(t, len(orgs), 2)
}
