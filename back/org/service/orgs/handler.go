package orgs

import (
	// "fmt"
	"context"
	"encoding/json"
	"log"
	"micro/config"
	"micro/db"
	"micro/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	// "net/http"
	// "fmt"
	// "micro/config"
	// "strings"
	// "os"
)

type OrgHandler struct {
	DB    db.OrgsDBLayer
	CACHE *redis.Client
}

type OrgHandlerInterface interface {
	//org
	AddOrg(c *gin.Context)
	GetOrg(c *gin.Context)
	GetOrgs(c *gin.Context)
	GetOrgsByType(c *gin.Context)
	DeleteOrg(c *gin.Context)
	UpdateOrg(c *gin.Context)
}

func NewOrgHandler() (OrgHandlerInterface, error) {
	CONF := config.DBConfig()
	//fmt.Printf("%#v", CONF)

	db, err := db.NewORM(CONF.DBconf())
	if err != nil {
		return nil, err
	}

	err = db.AutoMigrate(&models.Orgs{})
	if err != nil {
		return nil, err
	}

	cache := redis.NewClient(&redis.Options{
		Addr:     "anjanacache:6379",
		Password: "",
		DB:       0,
	})
	if err != nil {
		return nil, err
	}

	return &OrgHandler{
		DB:    db,
		CACHE: cache,
	}, nil

}

// ________________________________[[  ORGS ]]
// ________________[ ADD ORG ]
// @BasePath /v1/org/add
// org godoc
// @Summary add a new case in db
// @Schemes http
// @Description add a new case in db
// @Tags        org
// @Accept      json
// @Produce     json
// @Param  type query string true "organization type"
// @Param  email query string true "organization email"
// @Param  orgname query string true "orgnanization name"
// @Param  cookie query string true "cookie bearer token"
// @Success     200 {array} models.Orgs
// @Failure     400 {string} ERROR
// @Router      /v1/org/add [post]
func (h *OrgHandler) AddOrg(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "server database error", "stage": "0"})
		return
	}

	var org models.Orgs
	err := c.ShouldBindJSON(&org)
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusNotFound,
			gin.H{
				"error": err.Error(),
				"stage": "1",
			})
		return
	}

	log.Printf("[!] Updating cache")
	h.CACHE.Del(context.Background(), "Orgs")

	org, err = h.DB.AddOrg(org)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": "2"})
		return
	}
	log.Printf("Added %s org\n", org.OrgName)
	c.JSON(http.StatusOK, org)
}

// ________________[ DELETE ORG ]
// @BasePath /v1/org
// org godoc
// @Summary delete a case in db
// @Schemes http
// @Description delete a case in db
// @Tags        org
// @Accept      json
// @Produce     json
// @Param  email query string true "organization email"
// @Param  orgname query string true "orgnanization name"
// @Param  cookie query string true "cookie bearer token"
// @Success     200 {string} MSG OK
// @Failure     400 {string} ERROR
// @Router      /v1/org/delete [delete]
func (h *OrgHandler) DeleteOrg(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "server database error"})
		return
	}
	var req models.Orgs //get request
	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}
	event, err := h.DB.GetOrg(req)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	log.Printf("[!] Updating cache")
	h.CACHE.Del(context.Background(), "Orgs")

	res, err := h.DB.DeleteOrg(event)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}
	log.Printf("Delete %s event\n", res.OrgName)
	c.JSON(http.StatusOK, event)
}

// ________________[ GET ALL ORGS ]
// @BasePath /v1/org
// org godoc
// @Summary get all cases in db
// @Schemes http
// @Description get all cases in db
// @Tags        org
// @Accept      json
// @Produce     json
// @Param  cookie query string true "cookie bearer token"
// @Success     200 {array} models.Orgs
// @Failure     400 {string} ERROR
// @Router      /v1/org/all [get]
func (h *OrgHandler) GetOrgs(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "server database error"})
		return
	}
	// TODO look into cache first
	// var orgs models.Orgs
	ctx := context.Background()

	orgs_cache, err := h.CACHE.Get(ctx, "Orgs").Result()

	if err != nil {
		orgs_cache, err := h.DB.GetOrgs()
		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
			return
		}

		var jsn interface{}

		in, _ := json.Marshal(orgs_cache)
		json.Unmarshal(in, &jsn)
		data, _ := json.Marshal(jsn)

		err = h.CACHE.Set(ctx, "Orgs", string(data), 0).Err()
		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		}

		c.JSON(http.StatusOK, orgs_cache)

	} else {
		var orgs []models.Orgs

		json.Unmarshal([]byte(orgs_cache), &orgs)
		c.JSON(http.StatusOK, orgs)
	}

	// fmt.Printf("Found %d orgs\n", len(orgs))

	// c.JSON(http.StatusOK, orgs)
}

// ________________[ GET ALL ORGS BY TYPE ]
// @BasePath /v1/org
// org godoc
// @Summary get orgs by type
// @Schemes http
// @Description get orgs by type
// @Tags        org
// @Accept      json
// @Produce     json
// @Param  type query string true "organization type"
// @Param  cookie query string true "cookie bearer token"
// @Success     200 {array} []models.Orgs
// @Failure     400 {string} ERROR
// @Router      /v1/org/get/types [post]
func (h *OrgHandler) GetOrgsByType(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "server database error"})
		return
	}

	// var TYPE string //get request
	TYPE := c.PostForm("type")
	// err := c.ShouldBindJSON(&TYPE)
	if TYPE == "" {
		c.JSON(http.StatusNotFound, gin.H{"error": "Type is empty"})
		return
	}

	orgs, err := h.DB.GetOrgsByTYPE(TYPE)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}
	// c.JSON(http.StatusOK, orgs)
	c.JSON(http.StatusOK, orgs)
}

// ________________[ GET ORG ]
// @BasePath /v1/org
// org godoc
// @Summary get 1 org by type
// @Schemes http
// @Description get orgs by type
// @Tags        org
// @Accept      json
// @Produce     json
// @Param  type query string true "organization type"
// @Param  cookie query string true "cookie bearer token"
// @Success     200 {array} models.Orgs
// @Failure     400 {string} ERROR
// @Router      /v1/org/get [post]
func (h *OrgHandler) GetOrg(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "server database error"})
		return
	}

	var req models.Orgs
	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	org, err := h.DB.GetOrg(req)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, org)
}

// ________________[ UPDATE ORG ]
// @BasePath /v1/org
// org godoc
// @Summary updates an org
// @Schemes http
// @Description updates an org
// @Tags        org
// @Accept      json
// @Produce     json
// @Param  type query string true "organization type"
// @Param  id   query string true "organization id"
// @Param  cookie query string true "cookie bearer token"
// @Success     200 {string} MSG OK
// @Failure     400 {string} ERROR
// @Router      /v1/org/update/{id} [put]
func (h *OrgHandler) UpdateOrg(c *gin.Context) {
	if h.DB == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "server database error"})
		return
	}

	var req models.Orgs //get request
	err := c.ShouldBindJSON(&req)
	log.Printf("%+v \n", req)
	if err != nil {
		ERR := err.Error()
		c.JSON(http.StatusNotFound, gin.H{"error": ERR, "stage": 1})
		return
	}

	// req.ID , _ = strconv.ParseUint(c.Param("id"), 10, 64)
	// TODO get user from cookie

	req.ID, _ = strconv.Atoi(c.Param("id"))
	org, err := h.DB.GetOrgID(req)

	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": 2})
		return
	}

	// fmt.Printf("[!] type: %s", strings.joinreq.Email)
	org.OrgName = req.OrgName
	org.Email = req.Email
	org.Phone = req.Phone
	org.Description = req.Description
	org.Type = req.Type
	org.Social = req.Social

	res, err := h.DB.UpdateOrg(org)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error(), "stage": 3})
		return
	}

	// fmt.Println(c.GetRawData())
	c.JSON(http.StatusOK, res)
}
