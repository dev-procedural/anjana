package orgs

import (
	// "micro/middleware"

	"github.com/gin-gonic/gin"
)

func OrgsRouter(Orgs OrgHandlerInterface, r *gin.Engine) error {
	org := r.Group("/v1/org")
	// org.Use(middleware.AuthMiddleware())
	{
		org.GET("/all", Orgs.GetOrgs) // GET
		org.POST("/get", Orgs.GetOrg)
		org.POST("/get/types", Orgs.GetOrgsByType)
		org.POST("/add", Orgs.AddOrg)
		org.DELETE("/delete", Orgs.DeleteOrg)
		org.PUT("/update/:id", Orgs.UpdateOrg)
	}
	return nil

}
