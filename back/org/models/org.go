package models

import (
	// "gorm.io/gorm"
	"time"
)

type Orgs struct {
	// gorm.Model
	ID          int `gorm:"primary_key;autoIncrement;index" json:"id"`
	CreatedAt   time.Time
	OrgName     string  `json:"orgname,omitempty" gorm:"not null;unique;"`
	Description string  `json:"description"`
	Phone       *string `json:"phone" gorm:"unique" `
	Email       *string `json:"email" gorm:"unique" binding:"email"`
	Social      string  `json:"social"`
	Type        string  `json:"type" binding:"omitempty,oneof=unknown 'family legal' 'civil matter' 'criminal defence' 'civil rights' immigrant indigenous"`
}

func (*Orgs) TableName() string {
	return "anjana.Orgs"
}
