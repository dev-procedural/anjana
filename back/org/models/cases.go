package models

import (
	// "gorm.io/gorm"
	"time"
)

type Cases struct {
	// gorm.Model
	ID          int    `gorm:"primary_key;autoIncrement" json:"id"`
	UserEmail   string `json:"useremail" gorm:"unique" binding:"omitempty"`
	AgentEmail  string `json:"agentemail" `
	City        string `json:"city"`
	Date        string `json:"date"`
	Description string `json:"description"`
	Stage       string `json:"stage" binding:"omitempty"`
	Type        string `json:"type" binding:"omitempty,oneof=unknown 'family legal' 'civil matter' 'criminal defence' 'civil rights' immigrant indigenous"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

func (*Cases) TableName() string {
	return "anjana.Cases"
}
