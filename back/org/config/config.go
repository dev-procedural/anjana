package config

import (
	"fmt"
	"os"
)

type DBConf struct {
	DB_USER string
	DB_PASS string
	DB_URL  string
	DB_NAME string
	DB_PORT string
	DB_TYPE string //myslq | postgres | sqlite
}

type Service struct {
	MAIL_URL  string
	MAIL_PORT string
	AUTH_URL  string
	AUTH_PORT string
	ORGS_URL  string
	ORGS_PORT string
}

type ServiceConf struct {
	MAIL_URL                 string
	MAIL_PORT                string
	AUTH_URL                 string
	AUTH_PORT                string
	ORG_URL                  string
	ORG_PORT                 string
	MAIL_NEW_ENDPOINT        string
	AUTH_MIDDLEWARE_ENDPOINT string
}

func ServiceConfig() ServiceConf {
	return ServiceConf{
		MAIL_URL:                 os.Getenv("MAIL_URL"),
		MAIL_PORT:                os.Getenv("MAIL_PORT"),
		AUTH_URL:                 os.Getenv("AUTH_URL"),
		AUTH_PORT:                os.Getenv("AUTH_PORT"),
		ORG_URL:                  os.Getenv("ORG_URL"),
		ORG_PORT:                 os.Getenv("ORG_PORT"),
		MAIL_NEW_ENDPOINT:        os.Getenv("MAIL_NEW_ENDPOINT"),
		AUTH_MIDDLEWARE_ENDPOINT: os.Getenv("AUTH_MIDDLEWARE_ENDPOINT"),
	}
}

type OrgConf struct {
	ORG_PORT string
	ORG_URL  string
}

func DBConfig() DBConf {
	return DBConf{
		DB_USER: os.Getenv("DB_USER"),
		DB_NAME: os.Getenv("DB_NAME"),
		DB_URL:  os.Getenv("DB_URL"),
		DB_PASS: os.Getenv("DB_PASS"),
		DB_PORT: os.Getenv("DB_PORT"),
		DB_TYPE: os.Getenv("DB_TYPE"),
	}
}

func OrgConfig() OrgConf {
	return OrgConf{
		ORG_URL:  os.Getenv("ORG_URL"),
		ORG_PORT: os.Getenv("ORG_PORT"),
	}
}

func (c DBConf) DBconf() string {
	if c.DB_TYPE == "mysql" {
		return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", c.DB_USER, c.DB_PASS, c.DB_URL, c.DB_PORT, c.DB_NAME)
	} else if c.DB_TYPE == "postgres" {
		return fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", c.DB_USER, c.DB_PASS, c.DB_NAME, c.DB_URL, c.DB_PORT)
	} else {
		return "TYPE empty"
	}
}

func (c DBConf) TestDBconf() string {
	if c.DB_TYPE == "mysql" {
		return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", c.DB_USER, c.DB_PASS, c.DB_URL, c.DB_PORT, c.DB_NAME)
	} else if c.DB_TYPE == "postgres" {
		return fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", c.DB_USER, c.DB_PASS, c.DB_NAME, c.DB_URL, c.DB_PORT)
	} else {
		return "TYPE empty"
	}
}

func (c OrgConf) Orgconf() string {
	return fmt.Sprintf("%s:%s", c.ORG_URL, c.ORG_PORT)
}
