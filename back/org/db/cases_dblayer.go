package db

import (
	"micro/models"
)

type CasesDBLayer interface {
	//CASE
	AddCase(models.Cases) (models.Cases, error)
	GetCase(models.Cases) (models.Cases, error)
	GetCaseID(models.Cases) (models.Cases, error)
	GetCases() ([]models.Cases, error)
	DeleteCase(models.Cases) (models.Cases, error)
	UpdateCase(models.Cases) (models.Cases, error)
	GetTypeCases(string) ([]models.Cases, error)
	GetTypeCase(models.Cases) (models.Cases, error)
}

// ________________[ CASES ]
func (db *DBORM) AddCase(cases models.Cases) (models.Cases, error) {
	return cases, db.Create(&cases).Error
}

func (db *DBORM) UpdateCase(Case models.Cases) (models.Cases, error) {
	err := db.Model(&Case).Updates(models.Cases{
		ID:         Case.ID,
		UserEmail:  Case.UserEmail,
		Stage:      Case.Stage,
		AgentEmail: Case.AgentEmail}).Error
	return Case, err
}

func (db *DBORM) GetCases() (cases []models.Cases, err error) {
	return cases, db.Find(&cases).Error
}

func (db *DBORM) GetCase(Case models.Cases) (models.Cases, error) {
	err := db.Where(&models.Cases{ID: Case.ID, UserEmail: Case.UserEmail}).First(&Case).Error
	return Case, err
}

func (db *DBORM) GetTypeCase(Case models.Cases) (models.Cases, error) {
	return Case, db.Where("user_email = ?", Case.UserEmail).First(&Case).Error
	// return Case, db.Where("user_email = ? and type = ?", Case.UserEmail, Case.Type).First(&Case).Error
}

func (db *DBORM) GetTypeCases(T string) (Cases []models.Cases, err error) {
	return Cases, db.Where("type = ?", T).Find(&Cases).Error
}

func (db *DBORM) GetCaseID(Case models.Cases) (models.Cases, error) {
	err := db.Where(&models.Cases{ID: Case.ID}).Find(&Case).Error
	return Case, err
}

func (db *DBORM) DeleteCase(event models.Cases) (models.Cases, error) {
	err := db.Unscoped().Delete(&event).Error
	return event, err
}
