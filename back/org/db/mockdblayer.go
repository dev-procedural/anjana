package db

import (
	"encoding/json"
	// "fmt"
	// "strings"
	"micro/models"
)

type MockDBLayer struct {
	err   error
	Orgs  []models.Orgs
	Cases []models.Cases
}

func NewMockDBLayerWithData() *MockDBLayer {
	ORGS := `
[
    {
        "CreatedAt": "2022-07-01T19:27:29.028879Z",
        "description": "this a desc",
        "email": "qwe@gmail.com",
        "id": 1,
        "orgname": "org3",
        "phone": null
    },
    {
        "CreatedAt": "2022-07-01T19:27:38.957508Z",
        "description": "this a desc",
        "email": null,
        "id": 2,
        "orgname": "org1",
        "phone": "123123"
    }
]
`
	CASES := `
[
    {
        "CreatedAt": "2022-08-04T10:11:59.450062Z",
        "agentemail": "",
        "id": 1,
        "type": "immigrant",
        "useremail": "externaluser@gmail.com"
    },
    {
        "CreatedAt": "2022-08-04T10:32:03.335493Z",
        "agentemail": "julio.cesar.guerrero.olea@gmail.com",
        "id": 2,
        "type": "immigrant",
        "useremail": "balusi@mail.com"
    },
    {
        "CreatedAt": "2022-08-05T10:39:24.476196Z",
        "agentemail": "mushi.the.moshi.man@gmail.com",
        "id": 3,
        "type": "immigrant",
        "useremail": "baruyi@mail.com"
    }
]`

	var Orgs []models.Orgs
	var Cases []models.Cases
	json.Unmarshal([]byte(ORGS), &Orgs)
	json.Unmarshal([]byte(CASES), &Cases)

	return NewMockDBLayer(Orgs, Cases)
}

func NewMockDBLayer(Orgs []models.Orgs, Cases []models.Cases) *MockDBLayer {
	return &MockDBLayer{
		Orgs:  Orgs,
		Cases: Cases,
	}
}

func (mock *MockDBLayer) SetError(err error) {
	mock.err = err
}

func (mock *MockDBLayer) GetAllOrg() ([]models.Orgs, error) {
	return mock.Orgs, nil
}

func (mock *MockDBLayer) GetAllCases() ([]models.Cases, error) {
	return mock.Cases, nil
}

func (mock *MockDBLayer) AddOrg(org models.Orgs) (models.Orgs, error) {
	if mock.err != nil {
		return models.Orgs{}, mock.err
	}
	mock.Orgs = append(mock.Orgs, org)
	return org, nil
}

func (mock *MockDBLayer) AddCase(case_ models.Cases) (models.Cases, error) {
	if mock.err != nil {
		return models.Cases{}, mock.err
	}
	mock.Cases = append(mock.Cases, case_)
	return case_, nil
}
