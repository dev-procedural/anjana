package db

import (
	"micro/models"
)

type OrgsDBLayer interface {
	//ORG
	AddOrg(models.Orgs) (models.Orgs, error)
	GetOrg(models.Orgs) (models.Orgs, error)
	GetOrgID(models.Orgs) (models.Orgs, error)
	GetOrgs() ([]models.Orgs, error)
	GetOrgsByTYPE(string) ([]string, error)
	DeleteOrg(models.Orgs) (models.Orgs, error)
	UpdateOrg(models.Orgs) (models.Orgs, error)
}

// ________________[ ORG ]
func (db *DBORM) AddOrg(org models.Orgs) (models.Orgs, error) {
	err := db.Create(&org).Error
	return org, err
}

func (db *DBORM) UpdateOrg(org models.Orgs) (models.Orgs, error) {
	err := db.Model(&org).Updates(models.Orgs{OrgName: org.OrgName,
		Description: org.Description,
		Email:       org.Email,
		Phone:       org.Phone,
		Social:      org.Social,
		Type:        org.Type}).Error
	return org, err
}

func (db *DBORM) GetOrgs() (orgs []models.Orgs, err error) {
	return orgs, db.Find(&orgs).Error
}

func (db *DBORM) GetOrg(org models.Orgs) (models.Orgs, error) {
	err := db.Where(&models.Orgs{OrgName: org.OrgName}).Find(&org).Error
	return org, err
}

func (db *DBORM) GetOrgsByTYPE(T string) (Orgs []string, err error) {
	return Orgs, db.Table("Orgs").Pluck("email", &Orgs).Error
}

func (db *DBORM) GetOrgID(org models.Orgs) (models.Orgs, error) {
	err := db.Where(&models.Orgs{ID: org.ID}).Find(&org).Error
	return org, err
}

func (db *DBORM) DeleteOrg(event models.Orgs) (models.Orgs, error) {
	err := db.Unscoped().Delete(&event).Error
	return event, err
}
