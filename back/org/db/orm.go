package db

import (
	"database/sql"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type DBORM struct {
	*gorm.DB
}

func NewORM(config string) (*DBORM, error) {
	sqlDB, err := sql.Open("pgx", config)
	sqlDB.Exec(`set search_path='anjana'`)
	// defer sqlDB.Close()

	db, err := gorm.Open(postgres.New(
		postgres.Config{
			PreferSimpleProtocol: true,
			Conn:                 sqlDB}),
		&gorm.Config{})

	return &DBORM{DB: db}, err
}
