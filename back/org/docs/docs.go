// Package docs GENERATED BY SWAG; DO NOT EDIT
// This file was generated by swaggo/swag
package docs

import "github.com/swaggo/swag"

const docTemplate = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{escape .Description}}",
        "title": "{{.Title}}",
        "contact": {},
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/v1/case/add": {
            "post": {
                "description": "add new case and send notification, also this is the producer for the consumer",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "case"
                ],
                "summary": "add new case",
                "parameters": [
                    {
                        "type": "string",
                        "description": "unknown, family legal, civil matter, criminal defence, civil rights, immigrant, indigenous",
                        "name": "type",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "useremail",
                        "name": "useremail",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "cookie bearer token",
                        "name": "cookie",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.Cases"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/v1/case/get/type": {
            "post": {
                "description": "get only 1 case by type",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "case"
                ],
                "summary": "add new case",
                "parameters": [
                    {
                        "type": "string",
                        "description": "organization type",
                        "name": "type",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "useremail",
                        "name": "useremail",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "cookie bearer token",
                        "name": "cookie",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.Cases"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/v1/case/get/types": {
            "post": {
                "description": "get cases by type",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "case"
                ],
                "summary": "add new case",
                "parameters": [
                    {
                        "type": "string",
                        "description": "organization type",
                        "name": "type",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "useremail",
                        "name": "useremail",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "cookie bearer token",
                        "name": "cookie",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "type": "array",
                                "items": {
                                    "$ref": "#/definitions/models.Cases"
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/v1/case/update/{id}": {
            "put": {
                "description": "update case with agent",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "case"
                ],
                "summary": "update case",
                "parameters": [
                    {
                        "type": "string",
                        "description": "organization type",
                        "name": "type",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "useremail",
                        "name": "useremail",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "agentemail",
                        "name": "agent",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "cookie bearer token",
                        "name": "cookie",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.Cases"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/v1/org/add": {
            "post": {
                "description": "add a new case in db",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "org"
                ],
                "summary": "add a new case in db",
                "parameters": [
                    {
                        "type": "string",
                        "description": "organization type",
                        "name": "type",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "organization email",
                        "name": "email",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "orgnanization name",
                        "name": "orgname",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "cookie bearer token",
                        "name": "cookie",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.Orgs"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/v1/org/all": {
            "get": {
                "description": "get all cases in db",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "org"
                ],
                "summary": "get all cases in db",
                "parameters": [
                    {
                        "type": "string",
                        "description": "cookie bearer token",
                        "name": "cookie",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.Orgs"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/v1/org/delete": {
            "delete": {
                "description": "delete a case in db",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "org"
                ],
                "summary": "delete a case in db",
                "parameters": [
                    {
                        "type": "string",
                        "description": "organization email",
                        "name": "email",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "orgnanization name",
                        "name": "orgname",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "cookie bearer token",
                        "name": "cookie",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "string"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/v1/org/get": {
            "post": {
                "description": "get orgs by type",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "org"
                ],
                "summary": "get 1 org by type",
                "parameters": [
                    {
                        "type": "string",
                        "description": "organization type",
                        "name": "type",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "cookie bearer token",
                        "name": "cookie",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.Orgs"
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/v1/org/get/types": {
            "post": {
                "description": "get orgs by type",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "org"
                ],
                "summary": "get orgs by type",
                "parameters": [
                    {
                        "type": "string",
                        "description": "organization type",
                        "name": "type",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "cookie bearer token",
                        "name": "cookie",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "type": "array",
                                "items": {
                                    "$ref": "#/definitions/models.Orgs"
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/v1/org/update/{id}": {
            "put": {
                "description": "updates an org",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "org"
                ],
                "summary": "updates an org",
                "parameters": [
                    {
                        "type": "string",
                        "description": "organization type",
                        "name": "type",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "organization id",
                        "name": "id",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "cookie bearer token",
                        "name": "cookie",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "string"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "models.Cases": {
            "type": "object",
            "properties": {
                "agentemail": {
                    "type": "string"
                },
                "createdAt": {
                    "type": "string"
                },
                "id": {
                    "description": "gorm.Model",
                    "type": "integer"
                },
                "type": {
                    "type": "string",
                    "enum": [
                        "unknown",
                        "family legal",
                        "civil matter",
                        "criminal defence",
                        "civil rights",
                        "immigrant",
                        "indigenous"
                    ]
                },
                "useremail": {
                    "type": "string"
                }
            }
        },
        "models.Orgs": {
            "type": "object",
            "properties": {
                "createdAt": {
                    "type": "string"
                },
                "description": {
                    "type": "string"
                },
                "email": {
                    "type": "string"
                },
                "id": {
                    "description": "gorm.Model",
                    "type": "integer"
                },
                "orgname": {
                    "type": "string"
                },
                "phone": {
                    "type": "string"
                },
                "social": {
                    "type": "string"
                },
                "type": {
                    "type": "string",
                    "enum": [
                        "unknown",
                        "family legal",
                        "civil matter",
                        "criminal defence",
                        "civil rights",
                        "immigrant",
                        "indigenous"
                    ]
                }
            }
        }
    }
}`

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = &swag.Spec{
	Version:          "",
	Host:             "",
	BasePath:         "",
	Schemes:          []string{},
	Title:            "",
	Description:      "",
	InfoInstanceName: "swagger",
	SwaggerTemplate:  docTemplate,
}

func init() {
	swag.Register(SwaggerInfo.InstanceName(), SwaggerInfo)
}
