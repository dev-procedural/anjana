package main

import (
	"log"
	"micro/config"
	"micro/service/rest"
)

func main() {
	CONF := config.OrgConfig()
	API, _ := rest.RunAPI()

	log.Println("Main log...")
	log.Fatal(API.Run(CONF.Orgconf()))
}
