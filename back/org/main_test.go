package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"micro/models"
	"micro/service/rest"

	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOrgs(t *testing.T) {
	API, _ := rest.RunAPI()
	// ORG := rest.ExportORG()

	ts := httptest.NewServer(API)

	resp, err := http.Get(fmt.Sprintf("%s/v1/org/all", ts.URL))
	defer resp.Body.Close()
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	data, _ := ioutil.ReadAll(resp.Body)

	var orgs []models.Orgs
	json.Unmarshal(data, &orgs)

	log.Printf("%#v", len(orgs))
	assert.Equal(t, len(orgs), 2)

}
