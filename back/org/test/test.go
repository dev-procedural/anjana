package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"
	"github.com/nats-io/nats.go"
	//_ "github.com/jinzhu/gorm/dialects/mysql"
    // _ "github.com/jinzhu/gorm/dialects/postgres"

    // "github.com/jinzhu/gorm"
	// "micro/models"
)

func main() {
	r := gin.Default()
	r.Use(cors.AllowAll())
	// dns := 'anjana:anjana@tcp(http://localhost:3306)/anjana'
	// db, err := gorm.Open("postgres", "user=anjana password=anjana dbname=anjana host=192.168.91.2 port=5432 sslmode=disable")
	// // db, err := gorm.Open("postgres", "postgres://anjana:anjana@192.168.91.2:5432/anjana?sslmode=disable")
	// defer db.Close()

	// if err != nil {
	//   fmt.Println("Error on db conn")
	//   fmt.Println(err)
      // return 
	// }

	// fmt.Println("[+]",db.AutoMigrate(&models.OrgEvents{}))
	// fmt.Println("[+]",db.HasTable("OrgEvents"))



	r.POST("/ping", func(c *gin.Context) {
		a := make(map[string]string)
		c.ShouldBind(&a)

		nc, _ := nats.Connect("nats://172.17.0.2:4222")
		nc.Publish("foo", []byte(fmt.Sprint(a)))

		// Responding to a request message
		nc.Subscribe("foo", func(m *nats.Msg) {
				m.Respond([]byte("answer is 42"))
		})

		QQ ,_:= nc.Subscribe("foo", func(m *nats.Msg) {
			fmt.Printf("%T", m.Data)
		})

		c.JSON(200, gin.H{"msg": a, "queue": QQ })
		//take action
	})

	r.Run(":8889")
}
