# PASS
# add
echo "[!] add"
http localhost:8089/v1/org/add orgname="org_1" email="a@test.com" description="this is a desc" type="immigrant"
http localhost:8089/v1/org/add orgname="org_2" phone="123124" description="this is a desc" type="immigrant"
http localhost:8089/v1/org/add orgname="org_3" phone="123125" description="this is a desc" 
# update
echo "[!] update"
http localhost:8089/v1/org/update/1 orgname="org_1" email="a@changed.com" 

# FAIL
echo "[!] known fail"
http localhost:8089/v1/org/add orgname="org_4" phone="123126" type="immigrant"
http localhost:8089/v1/org/add orgname="org_4" type="immigrant"


