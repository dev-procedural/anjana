package db

import (
	"auth1/models"
)

type DBLayer interface {
	//ORG
	AddUser(models.Login)    (models.Login, error)
	GetUsers()               ([]models.Login, error)
	GetUser(models.Login)    (models.Login, error)
	GetUserID(models.Login)  (models.Login, error)
	DeleteUser(models.Login) (models.Login, error)
	UpdateUser(models.Login) (models.Login, error)
}
