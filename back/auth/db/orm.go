package db

import (
	// "fmt"
	"auth1/models"
	"gorm.io/gorm"
	"gorm.io/driver/postgres"
	"database/sql"
	"golang.org/x/crypto/bcrypt"
	// "log"
)

type DBORM struct {
	*gorm.DB
}

func NewORM(config  string) (*DBORM, error) {
	sqlDB, err := sql.Open("pgx", config)
	sqlDB.Exec(`set search_path='anjana'`)
  // defer sqlDB.Close()

	db, err := gorm.Open(postgres.New(
		postgres.Config{ 
			PreferSimpleProtocol: true,
			Conn : sqlDB}), 
		&gorm.Config{})

	return &DBORM{ DB: db, }, err
}

//________________[ USER ]
func (db *DBORM) AddUser(user models.Login) (models.Login, error) {
	pass := []byte(user.Password)
	hashed, err := bcrypt.GenerateFromPassword(pass, 2)

	user.Password = string(hashed)
	err = db.Create(&user).Error
	return user, err
}

func (db *DBORM) UpdateUser(user models.Login) (models.Login, error) {
	err := db.Model(&user).Updates(models.Login{ UserName: user.UserName, 
                                               Password: user.Password}).Error
	return user, err
}

func (db *DBORM) GetUsers() (user []models.Login, err error) {
	return user, db.Find(&user).Error
}

func (db *DBORM) GetUser(user models.Login) (models.Login, error) {
	err := db.Where(&models.Login{UserName: user.UserName}).First(&user).Error
	return user, err
}

func (db *DBORM) GetUserID(user models.Login) (models.Login, error) {
	err := db.Where(&models.Login{ID: user.ID}).First(&user).Error
	return user, err
}

func (db *DBORM) DeleteUser(user models.Login) (models.Login, error) {
	err := db.Unscoped().Delete(&user).Error
	return user, err
}

