package main

import (
	"auth1/config"
	"auth1/services/rest"

	"log"
)

func main() {

	CONF := config.LoginConfig()

	API, _ := rest.RunAPI()

	log.Println(CONF.Loginconf())

	log.Println("Main log...")
	log.Fatal(API.Run(CONF.Loginconf()))

}
