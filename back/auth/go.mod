module auth1

go 1.16

require (
	github.com/auth0/go-jwt-middleware/v2 v2.0.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.8.1
	github.com/gorilla/context v1.1.1
	github.com/gorilla/sessions v1.2.1
	github.com/gwatts/gin-adapter v1.0.0
	github.com/joho/godotenv v1.4.0
	github.com/rs/cors/wrapper/gin v0.0.0-20220619195839-da52b0701de5
	github.com/swaggo/files v0.0.0-20220728132757-551d4a08d97a
	github.com/swaggo/gin-swagger v1.5.2
	github.com/swaggo/swag v1.8.5
	golang.org/x/crypto v0.0.0-20220824171710-5757bc0c5503
	gorm.io/driver/postgres v1.3.9
	gorm.io/gorm v1.23.8
)
