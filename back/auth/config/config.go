package config

import (
	"fmt"
	"os"
)

type DBConf struct {
	DB_USER string
	DB_PASS string
	DB_URL  string
	DB_NAME string
	DB_PORT string
	DB_TYPE string //myslq | postgres | sqlite
}

type Service struct {
	MAIL_URL         string
	MAIL_PORT        string
	AUTH_URL         string
	AUTH_PORT        string
	ORGS_URL         string
	ORGS_PORT        string
	SENDGRID_API_KEY string
	SENDGRID_CNAME   string
}

func SVCConfig() Service {
	return Service{
		MAIL_URL:         os.Getenv("MAIL_URL"),
		MAIL_PORT:        os.Getenv("MAIL_PORT"),
		AUTH_URL:         os.Getenv("AUTH_URL"),
		AUTH_PORT:        os.Getenv("AUTH_PORT"),
		ORGS_URL:         os.Getenv("ORGS_URL"),
		ORGS_PORT:        os.Getenv("ORGS_PORT"),
		SENDGRID_API_KEY: os.Getenv("SENDGRID_API_KEY"),
		SENDGRID_CNAME:   os.Getenv("SENDGRID_CNAME"),
	}
}

type LoginConf struct {
	AUTH_PORT string
	AUTH_URL  string
}

func DBConfig() DBConf {
	return DBConf{
		DB_USER: os.Getenv("DB_USER"),
		DB_NAME: os.Getenv("DB_NAME"),
		DB_URL:  os.Getenv("DB_URL"),
		DB_PASS: os.Getenv("DB_PASS"),
		DB_PORT: os.Getenv("DB_PORT"),
		DB_TYPE: os.Getenv("DB_TYPE"),
	}
}

func LoginConfig() LoginConf {
	return LoginConf{
		AUTH_URL:  os.Getenv("AUTH_URL"),
		AUTH_PORT: os.Getenv("AUTH_PORT"),
	}
}

func (c DBConf) DBconf() string {
	if c.DB_TYPE == "mysql" {
		return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", c.DB_USER, c.DB_PASS, c.DB_URL, c.DB_PORT, c.DB_NAME)
	} else if c.DB_TYPE == "postgres" {
		return fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable",
			c.DB_USER, c.DB_PASS, c.DB_NAME, c.DB_URL, c.DB_PORT)
	} else {
		return "TYPE empty"
	}
}

func (c LoginConf) Loginconf() string {
	return fmt.Sprintf("%s:%s", c.AUTH_URL, c.AUTH_PORT)
}
