package rest

import (
	"auth1/models"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	// "auth1/db"

	_ "auth1/docs"

	"github.com/gin-gonic/gin"
	adapter "github.com/gwatts/gin-adapter"

	cors "github.com/rs/cors/wrapper/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	jwtmiddleware "github.com/auth0/go-jwt-middleware/v2"
	"github.com/auth0/go-jwt-middleware/v2/jwks"
	"github.com/auth0/go-jwt-middleware/v2/validator"
	"github.com/joho/godotenv"
)

func RunAPI() (*gin.Engine, error) {
	return RunAPIWithHandler(), nil
}

func RunAPIWithHandler() *gin.Engine {

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	//get user
	r := gin.Default()

	issuerURL, _ := url.Parse(os.Getenv("AUTH0_ISSUER_URL"))
	// audience := os.Getenv("AUTH0_AUDIENCE")
	provider := jwks.NewCachingProvider(issuerURL, time.Duration(5*time.Minute))

	jwtValidator, _ := validator.New(provider.KeyFunc,
		validator.RS256,
		issuerURL.String(),
		[]string{},
	)

	jwtMiddleware := jwtmiddleware.New(jwtValidator.ValidateToken)

	r.Use(gin.LoggerWithFormatter(models.CustomLog))
	r.Use(cors.AllowAll())

	user := r.Group("/v1/auth")
	// log.Printf("%#v <<<THIS", jwtMiddleware)
	user.Use(adapter.Wrap(jwtMiddleware.CheckJWT))
	{
		// user.POST("/version", login.AuthHandler())
		user.POST("/login", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{"message": "Authenticated"})
		})
	}

	url := ginSwagger.URL("https://auth.anjanahelp.org/auth/swagger/doc.json")
	r.GET("/auth/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return r

}
