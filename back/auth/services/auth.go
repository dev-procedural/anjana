package service

import (
	jwtmiddleware "github.com/auth0/go-jwt-middleware/v2"
	"github.com/auth0/go-jwt-middleware/v2/validator"
	"github.com/gin-gonic/gin"
)

type LoginHandler struct {
}

type LoginHandlerInterface interface {
	Validate(c *gin.Context)
}

func (h *LoginHandler) Validate(c *gin.Context) {
	claims := r.Context().Value(jwtmiddleware.ContextKey{}).(*validator.ValidatedClaims)
}
