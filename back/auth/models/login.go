package models

import "time"

//Login is a simple type to contain username and password
type Login struct {
	ID        int    `gorm:"primary_key;autoIncrement;index" json:"id"`
	Name      string `form:"name" json:"name" `
	LastName  string `form:"lastname" json:"lastname" `
	CreatedAt time.Time
	UserName  string `form:"username" json:"username" gorm:"unique"`
	Password  string `form:"password" json:"password" `
	Terms     string `form:"tos" json:"tos"`
}

func (*Login) TableName() string {
	return "anjana.Login"
}
