package models

type Cases struct {
	// gorm.Model
	ID         int    `json:"id"`
	UserEmail  string `json:"useremail"`
	AgentEmail string `json:"agentemail" `
	Type       string `json:"type"`
	CreatedAt  string `json:"CreatedAt"`
}

type Tag struct {
	FamilyLegal     string
	CivilMatter     string
	CriminalDefence string
	CivilRights     string
	Immigrant       string
	Indigenous      string
}

type Message struct {
	FROM string
	TO   string
}
