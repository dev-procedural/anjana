package db

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	// "database/sql"
	// "fmt"
	// "github.com/gin-gonic/gin"
	// "golang.org/x/crypto/bcrypt"
)

type DBORM struct {
	*sqlx.DB
}

func NewORM(config string) (*DBORM, error) {
	db, err := sqlx.Connect("postgres", config)
	return &DBORM{DB: db}, err
}

//________________[[ MAILS ]]
// func (db *DBORM) GetOrgs(Type string) (orgs []string, err error) {
// 	// ty := map[string]interface{}{"type": "civil rights"}
// 	return orgs, db.Select(&orgs, `SELECT email FROM "Orgs" WHERE type=$1`, Type)
// }

// func (db *DBORM) GetCases(Email string) (cases []string, err error) {
// 	// ty := map[string]interface{}{"type": "civil rights"}
// 	return cases, db.Select(&cases, `SELECT agent_email FROM "Cases" WHERE user_email=$1`, Email)
// }

// func (db *DBORM) GetOrgs(orgs models.Login) (models.Login, error) {
// 	err := db.Where(&models.Login{OrgsName: orgs.OrgsName}).First(&orgs).Error
// 	return orgs, err
// }

// func (db *DBORM) GetOrgsID(orgs models.Login) (models.Login, error) {
// 	err := db.Where(&models.Login{ID: orgs.ID}).First(&orgs).Error
// 	return orgs, err
// }
