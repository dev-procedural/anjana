package main

import (
	"encoding/json"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/sendgrid/sendgrid-go"
)

func ERR(e error) {
	if e != nil {
		panic(e)
	}
}

func UpdateConfig() {

	// SPAWNING NGROK
	// TODO add waiter service to ngrok
	// TODO create inbound parse if not exists
	o := exec.Command("bash", "-c", "./utils/ngrok_set.sh &")
	o.Start()

	log.Println("[!] NGROK Running in background")

	time.Sleep(5 * time.Second)

	out, err := o.Output()
	log.Println("[!] error", err.Error())
	log.Println("[!] ", out)

	file, err := os.ReadFile("./utils/ngrok.endpoint")
	log.Println(file)
	ERR(err)

	endpoint := string(file) + "/v1/mail/inbound"
	endpoint = strings.Replace(endpoint, "\n", "", -1)
	endpoint = strings.Replace(endpoint, "\"", "", -1)

	cname := string(os.Getenv("SENDGRID_CNAME"))
	cname = strings.Replace(cname, "\"", "", -1)

	// UPDATE EVENT HOOK
	apiKey := os.Getenv("SENDGRID_API_KEY")
	log.Println(apiKey)
	host := "https://api.sendgrid.com"

	request := sendgrid.GetRequest(apiKey, "/v3/user/webhooks/event/settings", host)
	request.Method = "PATCH"

	request.Body = []byte(`{ "enabled": true, "url": "` + endpoint + `"}`)

	response, err := sendgrid.API(request)
	ERR(err)

	request3 := sendgrid.GetRequest(apiKey, "/v3/user/webhooks/parse/settings", host)
	request3.Method = "GET"
	request3.Body = []byte(`{ "hostname":"` + cname + `", "url":"` + endpoint + `", "spam_check":false, "send_raw":false}`)

	response3, err := sendgrid.API(request3)
	ERR(err)
	log.Println(response.StatusCode)
	if response.StatusCode == 403 {
		log.Println("[!] Error: credentials not valid")
	}

	// GET CURRENT
	var buf map[string][]map[string]string
	json.Unmarshal([]byte(response3.Body), &buf)

	current_host := buf["result"][0]["hostname"]

	if current_host == cname {
		// update webhook
		log.Printf("%q", "/v3/user/webhooks/parse/settings/"+cname)
		request2 := sendgrid.GetRequest(apiKey, "/v3/user/webhooks/parse/settings/"+cname, host)
		request2.Method = "PATCH"
		request2.Body = []byte(`{ "url":"` + endpoint + `", "spam_check":false, "send_raw":false}`)

		response2, err := sendgrid.API(request2)
		ERR(err)

		log.Println("[!]", response2.Body)
		log.Println("[!]", response2.StatusCode)

	} else {

		request2 := sendgrid.GetRequest(apiKey, "/v3/user/webhooks/parse/settings", host)
		request2.Method = "POST"
		request2.Body = []byte(`{ "hostname":"` + cname + `", "url":"` + endpoint + `", "spam_check":false, "send_raw":false}`)

		response2, err := sendgrid.API(request2)
		ERR(err)

		log.Println("[!]", response2.Body)
		log.Println("[!]", response2.StatusCode)
	}
	// TODO create sender
	// https://docs.sendgrid.com/api-reference/sender-verification/create-verified-sender-request
}

func main() {

	UpdateConfig()

}
