package rest

import (
	"mail/config"
	"mail/db"
	"mail/models"
	. "mail/services/mail"

	_ "mail/docs"

	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func NewHandler() (MailHandlerInterface, error) {
	CONF := config.DBConfig()

	db, err := db.NewORM(CONF.DBconf())
	if err != nil {
		return nil, err
	}

	return &MailHandler{DB: db}, nil
}

func RunAPI() (*gin.Engine, error) {
	mail, err := NewHandler()
	if err != nil {
		return nil, err
	}

	return RunAPIWithHandler(mail), nil
}

// func RunMockAPI(address string) error {
// 	h := NewMockHandler()
// 	return RunAPIWithHandler(address, h)
// }

func RunAPIWithHandler(Mail MailHandlerInterface) *gin.Engine {

	//Get gin's default engine
	gin.ForceConsoleColor()
	r := gin.Default()

	r.Use(gin.Logger())
	r.Use(cors.AllowAll())
	r.Use(gin.LoggerWithFormatter(models.CustomLog))

	//mail private
	mail_private := r.Group("/v1/mail")
	// mail_private.Use(middleware.AuthMiddleware())
	{
		// mail_private.POST("/notify", Mail.SendNotification)
		mail_private.POST("/ping", Mail.Test)
	}

	// mail public facing
	mail_public := r.Group("/v1/mail")
	// mail_public.Use(middleware.AuthMiddleware())
	{
		mail_private.POST("/new", Mail.SendNewRequestBCC)
		mail_public.POST("/inbound", Mail.InboundHandler)
	}

	url := ginSwagger.URL("https://mail.anjanahelp.org/mail/swagger/doc.json")
	r.GET("/mail/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return r
}
