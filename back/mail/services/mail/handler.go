package mail

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"mail/config"
	"mail/db"
	"mail/models"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"text/template"

	"github.com/gin-gonic/gin"
	"github.com/sendgrid/sendgrid-go"
	in "github.com/sendgrid/sendgrid-go/helpers/inbound"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	// "github.com/jmoiron/sqlx"
)

type MailHandler struct {
	DB db.DBLayer
}

type MailHandlerInterface interface {
	//mail
	// SendNewRequest(c *gin.Context)
	Test(c *gin.Context)
	InboundHandler(c *gin.Context)
	SendNewRequestBCC(c *gin.Context)
}

// ________________________________[[  MAILS ]]
// ________________[ ADD MAIL ]
// @BasePath /v1/mail
// mail godoc
// @Summary Send email bcc
// @Schemes http
// @Description send email blinc carbon copy
// @Tags        mail
// @Accept      json
// @Produce     json
// @Param  type query string true "type"
// @Param  user query string true "user"
// @Param  agent query string true "agent"
// @Success     200 {string} MSG OK
// @Failure     400 {string} ERROR
// @Router      /v1/mail/new [post]
func (h MailHandler) SendNewRequestBCC(c *gin.Context) {

	var NEW_CASE models.Cases

	ERR := c.ShouldBindJSON(&NEW_CASE)
	if ERR != nil {
		log.Println("[?]", ERR)
		c.JSON(http.StatusNotFound, gin.H{"error": ERR.Error()})
		return
	}

	log.Printf("[!] %#v", NEW_CASE)

	EMAILS, ERR := GetOrgsByType(NEW_CASE.Type)
	if ERR != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": ERR.Error()})
	}

	if len(EMAILS) == 0 {
		log.Println("[?]", ERR)
		c.JSON(http.StatusNotFound, "No organization mails found")
		return
	}

	// TODO make it async, either a routine or a queue
	ERR = SendNotification(EMAILS, NEW_CASE.UserEmail, "NEW")
	if ERR != nil {
		c.JSON(http.StatusNotFound, ERR)
		return
	}

	c.JSON(http.StatusOK, gin.H{"msg": EMAILS})
	return

}

// ________________[ INBOUND ]
// @BasePath /v1/mail
// mail godoc
// @Summary process inbound email
// @Schemes http
// @Description process inbound email
// @Tags        mail
// @Accept      json
// @Produce     json
// @Param  raw query string true "raw request"
// @Success     200 {string} MSG OK
// @Failure     400 {string} ERROR
// @Router      /v1/mail/inbound [post]
func (h MailHandler) InboundHandler(c *gin.Context) {
	REQ_, _ := in.Parse(c.Request)
	FROM := REQ_.Envelope.From
	BODY := REQ_.TextBody
	CONF := config.ServiceConfig()
	SUBJ := REQ_.ParsedValues["subject"]

	// TODO id
	log.Printf("%#v", BODY)

	// PARAMS
	USEREMAIL := strings.Split(SUBJ, " ")
	USER_EMAIL := USEREMAIL[len(USEREMAIL)-1]
	log.Println("[!] useremail:", USER_EMAIL)
	log.Println("[!] from:", FROM)

	// check if our mail
	if USER_EMAIL == "" || FROM == "" || len(regexp.MustCompile("(?i)bounces.*anjanahelp.org@sendgrid.net").FindAllString(FROM, -1)) > 0 {
		log.Printf("[!] Exiting processing due to local mail or empty...")
		return
	}

	// body validation
	BODY_SPLIT := strings.Split(BODY, "\n")
	MATCH := []string{}

	for val, i := range strings.Split(BODY, "\n") {
		if !regexp.MustCompile(">").MatchString(BODY_SPLIT[val]) {
			MATCH = append(MATCH, i)
		}
	}
	FILTERED_LINES := strings.Join(MATCH, " ")
	CANCEL := regexp.MustCompile("(?i)"+CONF.CANCEL_WORDS).FindAllString(FILTERED_LINES, -1)
	COMPLETE := regexp.MustCompile("(?i)"+CONF.COMPLETE_WORDS).FindAllString(FILTERED_LINES, -1)
	TAKE := regexp.MustCompile("(?i)"+CONF.ACCEPT_WORDS).FindAllString(FILTERED_LINES, -1)

	switch {
	case len(CANCEL) > 0:
		CancelCase(FROM, BODY, USER_EMAIL)
	case len(TAKE) > 0:
		TakeCase(FROM, BODY, USER_EMAIL)
	case len(COMPLETE) > 0:
		CompleteCase(FROM, BODY, USER_EMAIL)
	default:
		log.Println("[!] Incorrect inbound wording")
	}

	return
}

func CancelCase(FROM, BODY, USER_EMAIL string) error {
	CONF := config.ServiceConfig()
	CANCEL_WORDS := CONF.CANCEL_WORDS

	BODY_SPLIT := strings.Split(BODY, "\n")
	MATCH := []string{}

	for val, i := range strings.Split(BODY, "\n") {
		if !regexp.MustCompile(">").MatchString(BODY_SPLIT[val]) {
			MATCH = append(MATCH, i)
		}
	}

	FILTERED_LINES := strings.Join(MATCH, " ")

	CANCELLATION := regexp.MustCompile(fmt.Sprintf("(?i)%s", CANCEL_WORDS)).FindAllString(FILTERED_LINES, -1)
	// TODO parametrize
	log.Println("[!] email ", USER_EMAIL)
	log.Println("[!] email ", CANCELLATION)

	// check case in db
	CASE, ERR := GetOrgByType(USER_EMAIL)
	if ERR != nil {
		return errors.New(ERR.Error())
	}

	log.Printf("[!] %#v", CASE["msg"])
	// update case to 1
	if !(CASE["msg"].AgentEmail == "1") || !(CASE["msg"].AgentEmail == "") {
		UpdateCase(CASE["msg"].ID, "1", "OPEN")
		return nil
	}

	return nil
}

func CompleteCase(FROM, BODY, USER_EMAIL string) error {
	CONF := config.ServiceConfig()
	COMPLETE_WORDS := CONF.COMPLETE_WORDS

	BODY_SPLIT := strings.Split(BODY, "\n")
	MATCH := []string{}

	for val, i := range strings.Split(BODY, "\n") {
		if !regexp.MustCompile(">").MatchString(BODY_SPLIT[val]) {
			MATCH = append(MATCH, i)
		}
	}

	FILTERED_LINES := strings.Join(MATCH, " ")

	COMPLETION := regexp.MustCompile(fmt.Sprintf("(?i)%s", COMPLETE_WORDS)).FindAllString(FILTERED_LINES, -1)
	// TODO parametrize
	log.Println("[!] email ", USER_EMAIL)
	log.Println("[!] email ", COMPLETION)

	// check case in db
	CASE, ERR := GetOrgByType(USER_EMAIL)
	if ERR != nil {
		return errors.New(ERR.Error())
	}

	log.Printf("[!] %#v", CASE["msg"])
	// update case to 1
	UpdateCase(CASE["msg"].ID, FROM, "COMPLETED")

	return nil
}

func TakeCase(FROM, BODY, USER_EMAIL string) error {
	// TODO parametrize

	// check on db if case taken
	CASE, ERR := GetOrgByType(USER_EMAIL)
	if ERR != nil {
		return errors.New(ERR.Error())
	}
	log.Printf("[1] take case %#v", CASE)

	if CASE["msg"].AgentEmail == "" || CASE["msg"].AgentEmail == "1" {
		log.Printf("[1] Case empty %#v", CASE)
		// Adding to case
		EMAILS, ERR := GetOrgsByType(CASE["msg"].Type)
		if ERR != nil {
			return errors.New(ERR.Error())
		}

		// update case
		UpdateCase(CASE["msg"].ID, FROM, "INPROGRESS")
		ERR = SendNotification(EMAILS, USER_EMAIL, "TAKEN")
		if ERR != nil {
			return errors.New(ERR.Error())
		}
	} else {
		// notify to other users in orgs
		EMAILS, ERR := GetOrgsByType(CASE["msg"].Type)
		if ERR != nil {
			return errors.New(ERR.Error())
		}

		log.Printf("[!] Notify Remainder of Users")
		ERR = SendNotification(EMAILS, USER_EMAIL, "TAKEN")
		if ERR != nil {
			return errors.New(ERR.Error())
		}
	}

	return nil

}

func UpdateCase(ID int, AGENT_EMAIL, STAGE string) error {
	JSON := fmt.Sprintf(`{"agentemail": "%s", "stage": "%s"}`, AGENT_EMAIL, STAGE)
	CONF := config.ServiceConfig()

	client := &http.Client{}

	log.Println(CONF.UpdateCase(strconv.Itoa(ID)))
	req, _ := http.NewRequest("PUT", CONF.UpdateCase(strconv.Itoa(ID)), strings.NewReader(JSON))
	// req.Header.Set("Cookie", "jwtTokenSession="+cookie)
	req.Header.Set("Content-Type", "application/json")
	// req.Header.Set("Flag", "true")

	resp, err := client.Do(req)
	if err != nil || resp.StatusCode == 404 {
		log.Printf("[!] %#v  or %#v ", err, resp.Body)
		return errors.New("Mail can't be sent")
	}

	var buf bytes.Buffer
	var RESULT map[string]map[string]string
	io.Copy(&buf, resp.Body)

	json.Unmarshal([]byte(buf.String()), &RESULT)
	log.Println("[!] updated result", RESULT)
	return nil

}

// ________________[ Notifications ]
func SendNotification(MAILS []string, USER_EMAIL string, MSG string) error {

	CONF := config.ServiceConfig()
	MAIL_SUBJECT := CONF.MAIL_SUBJECT
	MAIL_SENDER := CONF.MAIL_SENDER
	FROM := mail.NewEmail("ANJANA", MAIL_SENDER)

	var MSGBUF bytes.Buffer
	var MSGO string

	m := mail.NewV3Mail()
	// TODO fix mailing logic, stop using fan out
	switch {
	case MSG == "NEW":
		// TODO parametrize and use html template, plan accordingly
		MSGO = "Reply to this mail to help"
		NEWCASE_TPL, _ := template.ParseFiles("services/mail/templates/newcase.html")
		NEWCASE_TPL.Execute(&MSGBUF, map[string]string{"USER_EMAIL": USER_EMAIL, "MSG": MSGO})
	case MSG == "TAKEN":
		// TODO parametrize and use html template, plan accordingly
		MSGO = fmt.Sprintf("Case to help %s is taken. We'll notify if anything. ", USER_EMAIL)
		CASETAKEN_TPL, _ := template.ParseFiles("services/mail/templates/notifyorgs.html")
		CASETAKEN_TPL.Execute(&MSGBUF, map[string]string{"MSG": MSGO})
	}

	content := mail.NewContent("text/html", MSGBUF.String())

	m.AddContent(content)
	m.SetFrom(FROM)

	personalization := mail.NewPersonalization()
	TO_ := mail.NewEmail("anjana", "notifications@parse.anjanahelp.org")
	// TO_ := mail.NewEmail(MAILS[0], MAILS[0])
	personalization.AddTos(TO_)

	if len(MAILS) > 1 {
		for _, i := range MAILS {
			MM := mail.NewEmail("mail", i)
			personalization.AddBCCs(MM)
		}
	}

	personalization.Subject = MAIL_SUBJECT + " " + USER_EMAIL
	m.AddPersonalizations(personalization)

	// TODO make it async, either a routine or a queue
	request := sendgrid.GetRequest(os.Getenv("SENDGRID_API_KEY"), "/v3/mail/send", "https://api.sendgrid.com")
	request.Method = "POST"
	request.Body = mail.GetRequestBody(m)
	_, err := sendgrid.API(request)
	if err != nil {
		log.Println("[?]", err)
		//c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return errors.New(err.Error())
	}

	//c.JSON(http.StatusOK, gin.H{"msg": response})
	return nil
}

func (h *MailHandler) Test(c *gin.Context) {

	res := make(map[string]string)
	c.ShouldBind(&res)

	var MSGBUF bytes.Buffer
	NEWCASE_TPL, _ := template.ParseFiles("services/mail/templates/newcase.html")
	log.Println(os.Getwd())

	NEWCASE_TPL.Execute(&MSGBUF, map[string]string{"USER_EMAIL": "Chubusu", "MSG": "MMMM"})

	// c.JSON(http.StatusOK, gin.H{"msg": MSGBUF.String()})

}

func GetOrgByType(USER_EMAIL string) (RESPONSE map[string]models.Cases, ERR error) {

	// TODO validate auth middleware
	// cookie, _ := c.Cookie("jwtTokenSession")
	CONF := config.ServiceConfig()

	client := &http.Client{}
	JSON := fmt.Sprintf(`{"useremail": "%s"}`, USER_EMAIL)

	req, _ := http.NewRequest("POST", CONF.CASE_GET_TYPE_ENDPOINT, strings.NewReader(JSON))
	// req.Header.Set("Cookie", "jwtTokenSession="+cookie)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Flag", "true")

	resp, ERR := client.Do(req)
	if ERR != nil || resp.StatusCode == 404 {
		if ERR != nil {
			return RESPONSE, ERR
		}
		return RESPONSE, errors.New("StatusCode 404")
	}

	var BUF bytes.Buffer
	io.Copy(&BUF, resp.Body)

	json.Unmarshal([]byte(BUF.String()), &RESPONSE)

	return RESPONSE, nil
}

func GetOrgsByType(TYPE string) (RESPONSE []string, ERR error) {

	JSON := url.Values{}
	JSON.Add("type", TYPE)
	CONF := config.ServiceConfig()

	client := &http.Client{}

	req, _ := http.NewRequest("POST", CONF.ORG_GET_TYPES_ENDPOINT, strings.NewReader(JSON.Encode()))

	// req.Header.Set("Cookie", "jwtTokenSession="+cookie)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, ERR := client.Do(req)
	if ERR != nil || resp.StatusCode == 404 {
		if ERR != nil {
			return RESPONSE, ERR
		}
		return RESPONSE, errors.New("StatusCode 404")
	}

	var BUF bytes.Buffer
	io.Copy(&BUF, resp.Body)

	json.Unmarshal(BUF.Bytes(), &RESPONSE)

	return RESPONSE, nil
}
