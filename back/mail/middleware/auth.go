package middleware

import (
	"io/ioutil"
	"log"
	"mail/config"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		cookie, err := c.Cookie("jwtTokenSession")
		cookie = strings.Split(c.Request.Header["Authorization"][0], " ")[1]

		// if not cookie bounce
		if cookie == "" || err != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": "jwtTokenSession not present"})
			c.AbortWithStatus(http.StatusNotFound)
			log.Println("[!] Error, no cookie preesent")
		} else {

			CONF := config.ServiceConfig()
			AUTH_ENDPOINT := CONF.AUTH_MIDDLEWARE_ENDPOINT

			client := &http.Client{}

			req, _ := http.NewRequest("POST", AUTH_ENDPOINT, nil)
			req.Header.Set("Cookie", "jwtTokenSession="+cookie)
			req.Header.Set("Content-Type", "application/json")
			req.Header.Set("Authorization", "Bearer "+COOKIE)
			resp, err := client.Do(req)

			if err != nil {
				c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
				c.Abort()
			}

			data, err := ioutil.ReadAll(resp.Body)

			if resp.StatusCode == 404 {
				c.JSON(http.StatusNotFound, gin.H{"error": data})
				c.Abort()
			}

			flag := false
			for _, cookie := range resp.Cookies() {
				if cookie.Name == "jwtTokenSession" {
					flag = true
				}
			}

			if flag == false {
				c.JSON(http.StatusNotFound, gin.H{"error": "invalid token"})
				c.Abort()
			}

		}

		if c.Request.Method != "OPTIONS" {
			c.Next()
		} else {
			c.AbortWithStatus(http.StatusOK)
		}
	}
}
