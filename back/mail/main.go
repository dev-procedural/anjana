package main

import (
	"log"
	"mail/config"
	"mail/services/rest"
)

func main() {
	CONF := config.MailConfig()
	CONF1 := config.ServiceConfig()
	API, _ := rest.RunAPI()

	log.Printf("Main log.... %#v \n%#v", CONF, CONF1)
	log.Fatal(API.Run(CONF.Mailconf()))
}
