package config

import (
	"fmt"
	"os"
)

type DBConf struct {
	DB_USER string
	DB_PASS string
	DB_URL  string
	DB_NAME string
	DB_PORT string
	DB_TYPE string //myslq | postgres | sqlite
}

type MailConf struct {
	MAIL_PORT string
	MAIL_URL  string
}

type ServiceConf struct {
	MAIL_URL                 string
	MAIL_PORT                string
	AUTH_URL                 string
	AUTH_PORT                string
	ORG_URL                  string
	ORG_PORT                 string
	SENDGRID_API_KEY         string
	SENDGRID_CNAME           string
	NGROK                    string
	CASE_UPDATE_ENDPOINT     string
	CASE_GET_TYPE_ENDPOINT   string
	ORG_GET_TYPES_ENDPOINT   string
	ORG_GET_TYPE_ENDPOINT    string
	CANCEL_WORDS             string
	COMPLETE_WORDS           string
	ACCEPT_WORDS             string
	MAIL_SUBJECT             string
	MAIL_SENDER              string
	AUTH_MIDDLEWARE_ENDPOINT string
}

func ServiceConfig() ServiceConf {
	return ServiceConf{
		MAIL_URL:                 os.Getenv("MAIL_URL"),
		MAIL_PORT:                os.Getenv("MAIL_PORT"),
		AUTH_URL:                 os.Getenv("AUTH_URL"),
		AUTH_PORT:                os.Getenv("AUTH_PORT"),
		ORG_URL:                  os.Getenv("ORG_URL"),
		ORG_PORT:                 os.Getenv("ORG_PORT"),
		SENDGRID_API_KEY:         os.Getenv("SENDGRID_API_KEY"),
		SENDGRID_CNAME:           os.Getenv("SENDGRID_CNAME"),
		NGROK:                    os.Getenv("NGROK"),
		CASE_UPDATE_ENDPOINT:     os.Getenv("CASE_UPDATE_ENDPOINT"),
		CASE_GET_TYPE_ENDPOINT:   os.Getenv("CASE_GET_TYPE_ENDPOINT"),
		ORG_GET_TYPES_ENDPOINT:   os.Getenv("ORG_GET_TYPES_ENDPOINT"),
		ORG_GET_TYPE_ENDPOINT:    os.Getenv("ORG_GET_TYPE_ENDPOINT"),
		CANCEL_WORDS:             os.Getenv("CANCEL_WORDS"),
		COMPLETE_WORDS:           os.Getenv("COMPLETE_WORDS"),
		ACCEPT_WORDS:             os.Getenv("ACCEPT_WORDS"),
		MAIL_SUBJECT:             os.Getenv("MAIL_SUBJECT"),
		MAIL_SENDER:              os.Getenv("MAIL_SENDER"),
		AUTH_MIDDLEWARE_ENDPOINT: os.Getenv("AUTH_MIDDLEWARE_ENDPOINT"),
	}
}

func DBConfig() DBConf {
	return DBConf{
		DB_USER: os.Getenv("DB_USER"),
		DB_NAME: os.Getenv("DB_NAME"),
		DB_URL:  os.Getenv("DB_URL"),
		DB_PASS: os.Getenv("DB_PASS"),
		DB_PORT: os.Getenv("DB_PORT"),
		DB_TYPE: os.Getenv("DB_TYPE"),
	}
}

func MailConfig() MailConf {
	return MailConf{
		MAIL_URL:  os.Getenv("MAIL_URL"),
		MAIL_PORT: os.Getenv("MAIL_PORT"),
	}
}

func (c DBConf) DBconf() string {
	if c.DB_TYPE == "mysql" {
		return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", c.DB_USER, c.DB_PASS, c.DB_URL, c.DB_PORT, c.DB_NAME)
	} else if c.DB_TYPE == "postgres" {
		// return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", c.DB_USER, c.DB_PASS, c.DB_URL, c.DB_PORT, c.DB_NAME)
		return fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable search_path=anjana", c.DB_USER, c.DB_PASS, c.DB_NAME, c.DB_URL, c.DB_PORT)
	} else {
		return "TYPE empty"
	}
}

func (c MailConf) Mailconf() string {
	return fmt.Sprintf("%s:%s", c.MAIL_URL, c.MAIL_PORT)
}

func (c ServiceConf) UpdateCase(CASE_ID string) string {
	return fmt.Sprintf("%s/%s", c.CASE_UPDATE_ENDPOINT, CASE_ID)
}
