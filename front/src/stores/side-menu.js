import { defineStore } from "pinia";

export const useSideMenuStore = defineStore("sideMenu", {
  state: () => ({
    menu: [
      {
        icon: "HomeIcon",
        pageName: "home",
        title: "Home",
      },
      {
        icon: "EditIcon",
        pageName: "top-menu-case-new",
        title: "New Case",
      },
      // {
      //   icon: "EditIcon",
      //   pageName: "top-menu-page-2",
      //   title: "Edit Profile",
      // }
    ],
  }),
});
