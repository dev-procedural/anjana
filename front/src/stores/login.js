import { defineStore } from "pinia";

export const loginStore = defineStore("login", {
  state: () => ({
    loginValue: localStorage.getItem("login"),
  }),
  getters: {
    loginGet(state) {
      if (localStorage.getItem("login") === null) {
        localStorage.setItem("login", "");
      }

      return state.loginValue;
    },
  },
  actions: {
    setLogin(loginGet) {
      localStorage.setItem("darkMode", loginGet);
      this.loginValue = loginGet;
    },
  },
});
