import { createApp } from "vue";
import { createPinia } from "pinia";
import { createAuth0 } from "@auth0/auth0-vue";
import App from "./App.vue";
import router from "./router";
import globalComponents from "./global-components";
import utils from "./utils";
import "./assets/css/app.css";
import VueCookies from 'vue3-cookies'

const app = createApp(App).use(router).use(createPinia());

app.use(VueCookies, {
    expireTimes: "1h",
    path: "/",
    domain: ".anjanahelp.org",
    secure: true,
    sameSite: "None"
});

app.use(createAuth0({
    domain:        import.meta.env.VITE_DOMAIN,
    client_id:     import.meta.env.VITE_CLIENT_ID,
    redirect_uri:  window.location.origin
 })
)

globalComponents(app);
utils(app);
router.app = app

app.mount("#app");

