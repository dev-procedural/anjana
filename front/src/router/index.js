import { createRouter, createWebHistory } from "vue-router";
import { authGuard, useAuth0 } from '@auth0/auth0-vue';
import TopMenu     from "../layouts/top-menu/Main.vue";
import Profile     from "../views/profile/Main.vue";
import Home        from "../views/home/Main.vue";
import Login       from "../views/login/Main.vue";
import Register    from "../views/register/Main.vue";
import Wizard      from "../views/wizard-layout-3/Main.vue";
import NotFound    from "../views/error/Main.vue";

import Axios from 'axios';
import dom from "@left4code/tw-starter/dist/js/dom";

import { useCookies } from "vue3-cookies";

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/case",
    component: TopMenu,
    children: [
      {
        path: "new",
        name: "top-menu-case-new",
        component: Wizard,
        beforeEnter: [authGuard]
      }
    ]
  },
  {
    path: '/:catchAll(.*)',
    name: "Notfound",
    component: NotFound
  },

];

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return savedPosition || { left: 0, top: 0 };
  },
});

export default router;
